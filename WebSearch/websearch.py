import os
import yaml
import urllib.parse
from libqtile.utils import send_notification
from libqtile.lazy import lazy


class WebSearch:
    engines_list = '/home/vikingkong/.dotfiles/config/qtile/engines.yml'

    @classmethod
    def run_search(cls):
        @lazy.function
        def __inner(qtile):
            try:
                with open(cls.engines_list) as f:
                    engines = yaml.load(f, Loader=yaml.FullLoader)
                    engines_str = '\n'.join(['{key} ({title})'.format(key=key, title=engines[key]['title']) for key in engines.keys()])
                    engine = os.popen('echo "' + engines_str + '" | dmenu -b -fn "Ubuntu Medium-14"').read()
                    if not engine:
                        return
                    if engine.strip('\n').split(' ')[0] not in engines:
                        search_str = engine.strip('\n')
                        engine = list(engines.keys())[0]
                    else:
                        search_str = os.popen('echo | dmenu -p "Search for:" -b -fn "Ubuntu Medium-14"').read().strip('\n')
                        engine = engine.strip('\n').split(' ')[0]
                    if not search_str:
                        return
                    if search_str == 'jj':
                        os.popen('setxkbmap ru')
                        search_str = os.popen('echo | dmenu -p "Search for:" -b -fn "Ubuntu Medium-14"').read().strip('\n')
                        os.popen('setxkbmap us')
                        if not search_str:
                            return
                    os.popen('firefox -new-tab "{url}"'.format(url=engines[engine]['url'].replace('%s', urllib.parse.quote(search_str))))
            except BaseException as e:
                send_notification('error', str(e), urgent=True)
        return __inner
