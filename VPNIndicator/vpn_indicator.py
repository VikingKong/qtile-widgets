import os
from libqtile.widget import base


class VPNIndicator(base.ThreadPoolText):
    orientations = base.ORIENTATION_HORIZONTAL

    def __init__(self, **config):
        base.ThreadPoolText.__init__(self, "", **config)

    def get_VPN(self):
        open_vpn = os.popen(r"nmcli -t -f NAME,TYPE c s -a|grep 'vpn$'|grep -Po '^[a-zA-Z\s]*'").read()
        open_vpn = open_vpn and 'OpenVPN: ' + open_vpn
        wireguard = os.popen(r"""sudo wg|grep 'interface'|awk '{OFS = ": ";print $2}'""").read()
        wireguard = wireguard and 'Wireguard: ' + wireguard
        vpn = open_vpn or wireguard
        if vpn == "":
            vpn = "VPN: Disconnected"
        return vpn.strip()

    def poll(self):
        return self.get_VPN()
