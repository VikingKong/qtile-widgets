from libqtile import bar
from libqtile.widget import base
from libqtile.utils import send_notification
from libqtile.lazy import lazy
import os
import re
import yaml


class TeaTimer(base._TextBox):

    defaults = [('play_sound', True), ('sound', '/usr/share/sounds/freedesktop/stereo/complete.oga'), ('player', 'mpv'),
                ('steep_list', '/home/vikingkong/.config/qtile/steeps.yml')]

    def __init__(self, widget=bar.CALCULATED, **config):
        base._TextBox.__init__(self, '', widget, **config)
        self.add_defaults(TeaTimer.defaults)
        self.is_counting = False
        self.default_text = "Tea Timer"
        self.text = self.default_text
        self.__call_later_funcs = []
        self.add_callbacks({'Button1': self.run_timer})
        self.add_callbacks({'Button3': self.run_next_steep})
        self.timer_minutes = 0
        self.timer_seconds = 0
        self.steep_index = 0
        self.steeps = {}
        self.tea = ''

    def run_timer(self):
        try:
            with open(self.steep_list) as f:
                self.steeps = yaml.load(f, Loader=yaml.FullLoader)
                tea_str = '\n'.join(self.steeps.keys())
                if not self.is_counting:
                    self.tea = os.popen('echo "' + tea_str + '" | dmenu -b -fn "Ubuntu Medium-14"').read().strip('\n')
                    if not self.tea:
                        return
                    steep_str = '\n'.join(self.steeps[self.tea])
                    steep = os.popen('echo "' + steep_str + '" | dmenu -b -fn "Ubuntu Medium-14"').read().strip('\n')
                    if not steep:
                        return
                    self.steep_index = self.steeps[self.tea].index(steep)
                    self.parse_steep(steep)
                    self.is_counting = True
                    self.update()
                else:
                    self.__reset()
        except BaseException as e:
            send_notification('error', str(e), urgent=True)

    def run_timer_keyboard(self):
        @lazy.function
        def __inner(qtile):
            self.run_timer()
        return __inner

    def run_next_steep(self):
        if (not self.is_counting) and self.tea:
            try:
                next_steep = self.steeps[self.tea][self.steep_index + 1]
                self.steep_index += 1
            except BaseException:
                next_steep = self.steeps[self.tea][self.steep_index]

            self.parse_steep(next_steep)
            self.is_counting = True
            self.update()
        else:
            return

    def run_next_steep_keyboard(self):
        @lazy.function
        def __inner(qtile):
            self.run_next_steep()
        return __inner

    def parse_steep(self, steep):
        self.timer_minutes = self.get_value(re.search(r"([\d]+)m", steep))
        self.timer_seconds = self.get_value(re.search(r"([\d]+)s", steep))

    def get_value(self, val):
        res = 0
        if val:
            try:
                res = int(val.group(1))
            except BaseException:
                pass
        return res

    def suffix(self, num):
        if num == 1:
            return 'st'
        elif num == 2:
            return 'nd'
        elif num == 3:
            return 'rd'
        else:
            return 'th'

    def update(self):
        if not self.is_counting:
            return

        if self.timer_seconds == 0:
            if self.timer_minutes == 0:
                send_notification(
                    'Tea cooker',
                    '{tea} {steep_number}{suffix} steep is ready!'.format(
                        tea=self.tea,
                        steep_number=self.steep_index + 1,
                        suffix=self.suffix(
                            self.steep_index + 1)),
                    urgent=True)
                if self.play_sound:
                    os.popen('{player} {sound}'.format(player=self.player, sound=self.sound))
                self.__reset()
                return
            else:
                self.timer_minutes -= 1
                self.timer_seconds = 59
        else:
            self.timer_seconds -= 1

        self.text = '{tea} {steep_number}{suffix} steep: {minutes}m {seconds}s left'.format(tea=self.tea,
                                                                                            steep_number=self.steep_index + 1,
                                                                                            suffix=self.suffix(self.steep_index + 1),
                                                                                            minutes=self.timer_minutes,
                                                                                            seconds=self.timer_seconds)
        self.bar.draw()
        func = self.timeout_add(1, self.update)
        self.__call_later_funcs.append(func)

    def __reset(self):
        self.text = self.default_text
        self.bar.draw()
        self.is_counting = False
        for f in self.__call_later_funcs:
            f.cancel()
        self.__call_later_funcs = []
