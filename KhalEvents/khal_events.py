import os
from libqtile.widget import base


class KhalEvents(base.ThreadPoolText):
    orientations = base.ORIENTATION_HORIZONTAL

    def __init__(self, **config):
        base.ThreadPoolText.__init__(self, "", **config)
        self.number_of_events = 0
        self.counter = 0
        self.event_list = []
        self.defaults = [('mouse_callbacks', {'Button1': self.next_event}), ('foreground', 'FFFF33'), ('update_interval', 600)]
        self.add_defaults(self.defaults)

    def get_events(self):
        def make_events_per_day(day_arr):
            date = day_arr[0]
            return ['{date}: {item}'.format(date=date, item=item) for item in day_arr[1:-1]]
        event_list = os.popen('khal list today --day-format "#{name}"').read()
        self.counter = 0
        if event_list != 'No events\n':
            event_list = event_list.split('#')[1:]
            event_list = [make_events_per_day(day.split('\n')) for day in event_list]
            event_list = [item for sublist in event_list for item in sublist]
            self.number_of_events = len(event_list)
            self.event_list = ["{item} ({i}/{l})".format(item=item, i=i+1, l=self.number_of_events)
                               for item, i in zip(event_list, range(self.number_of_events))]
        else:
            return event_list.strip()
        return self.event_list[self.counter]

    def next_event(self):
        if self.text != 'No events' and self.number_of_events > 1:
            self.counter = self.counter + 1
            if self.counter > self.number_of_events - 1:
                self.counter = 0
            self.update(self.event_list[self.counter])

    def poll(self):
        return self.get_events()
