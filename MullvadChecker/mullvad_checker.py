import requests
from libqtile.widget import base


class MullvadChecker(base.ThreadPoolText):
    orientations = base.ORIENTATION_HORIZONTAL

    def __init__(self, **config):
        base.ThreadPoolText.__init__(self, "", **config)

    def check(self):
        try:
            response = requests.get('https://am.i.mullvad.net/json').json()
            connected = response['mullvad_exit_ip']
            if connected:
                endpoint = response['mullvad_exit_ip_hostname']
                vpn_type = response['mullvad_server_type']
                result = vpn_type + ': ' + endpoint
                self.foreground = '#ffffff'
            else:
                result = 'Not connected to Mullvad'
                self.foreground = '#ff0000'
        except BaseException:
            result = 'Network is down'
            self.foreground = '#ff0000'
        return result

    def poll(self):
        return self.check()
