import os
import io
from libqtile.widget import base
from libqtile.lazy import lazy


class UniversalVolume(base.ThreadPoolText):
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [('limit', 100), ('show_name', True)]

    def __init__(self, **config):
        base.ThreadPoolText.__init__(self, "", **config)
        self.add_defaults(UniversalVolume.defaults)

    def parse_sinks(self):
        sinks_str = os.popen('pactl list sinks').read().split('Sink #')
        sinks = []
        for sink in sinks_str:
            current_parse = 'by_semicolon'
            if sink != '':
                current_sink = {'Properties': {}, 'Ports': {}, 'Formats': []}
                leaf = current_sink
                buf = io.StringIO(sink)
                buf.readline()
                while(True):
                    value = buf.readline()
                    if value:
                        pair = value.strip().split(':')
                        if len(pair) == 1 and current_parse != 'by_equal_sign' and current_parse != 'plain':
                            pair = pair[0].split(' ')
                            current_sink[pair[0].strip()] = pair[1].strip()
                        if pair[0] == 'Properties':
                            current_parse = 'by_equal_sign'
                            leaf = current_sink['Properties']
                        elif pair[0] == 'Ports':
                            current_parse = 'by_semicolon'
                            leaf = current_sink['Ports']
                        elif pair[0] == 'Formats':
                            current_parse = 'plain'
                        elif current_parse == 'by_semicolon':
                            leaf[pair[0].strip()] = ':'.join(pair[1:]).strip()
                        elif current_parse == 'by_equal_sign':
                            pair = value.strip().split('=')
                            leaf[pair[0].strip()] = pair[1].strip()
                        elif current_parse == 'plain':
                            current_sink['Formats'].append(value.strip())
                    else:
                        break
                sinks.append(current_sink)
        return sinks

    def get_volume(self):
        sinks = self.parse_sinks()
        res_sink = sinks[0]
        headset_name = ''
        isHeadset = False
        for sink in sinks:
            if sink['Properties']['device.bus'] == '"bluetooth"' and sink['Properties']['device.form_factor'] == '"headset"':
                isHeadset = True
                res_sink = sink
                headset_name = sink['Properties']['device.description'].replace('"', '')
                break
        if res_sink['Mute'] == 'yes':
            volume = 'M'
        else:
            volume_string = res_sink['Volume']
            volumes = volume_string.split(',')
            volume_left = volumes[0].split('/')[1].strip()[0:-1]
            volume_right = volumes[1].split('/')[1].strip()[0:-1]
            volume = int((int(volume_left)+int(volume_right))/2)
            if volume > self.limit and isHeadset is True:
                os.popen('pactl set-sink-volume {name} {limit}%'.format(name=res_sink['Name'], limit=self.limit))
        if volume != 'M':
            volume = str(volume)+'%'
        if headset_name != '' and self.show_name is True:
            result = '{headset_name}: {volume}'.format(headset_name=headset_name, volume=volume)
            self.foreground = "#0000cc"
        else:
            result = 'Volume: {volume}'.format(volume=volume)
            self.foreground = "#ffffff"
        return result

    @classmethod
    def change_volume(cls, command):
        @lazy.function
        def __inner(qtile):
            sinks = cls.parse_sinks(cls)
            res_sink = sinks[0]
            commands = {'up': 'amixer -c 0 -q set Master 2dB+', 'down': 'amixer -c 0 -q set Master 2dB-'}
            for sink in sinks:
                if sink['Properties']['device.bus'] == '"bluetooth"' and sink['Properties']['device.form_factor'] == '"headset"':
                    res_sink = sink
                    commands['up'] = "pactl set-sink-volume "+res_sink['Name']+' +2%'
                    commands['down'] = "pactl set-sink-volume "+res_sink['Name']+' -2%'
                    break
            os.popen(commands[command])
        return __inner

    @staticmethod
    def mute_volume():
        @lazy.function
        def __inner(qtile):
            os.popen('amixer -D pulse set Master toggle')
        return __inner

    def poll(self):
        return self.get_volume()
